#![cfg_attr(not(feature = "std"), no_std)]


use frame_support::{debug, decl_module, decl_storage, decl_event, decl_error, dispatch};
use frame_system::ensure_signed;

use sp_runtime::traits::Printable;
use sp_runtime::print;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

/// Configure the pallet by specifying the parameters and types on which it depends.
pub trait Trait: frame_system::Trait {
	/// Because this pallet emits events, it depends on the runtime's definition of an event.
	type Event: From<Event<Self>> + Into<<Self as frame_system::Trait>::Event>;
}

decl_storage! {
	trait Store for Module<T: Trait> as TemplateModule {
		Something get(fn something): Option<u32>;
	}
}

decl_event!(
	pub enum Event<T> where AccountId = <T as frame_system::Trait>::AccountId {
		SomethingStored(u32, AccountId),
	}
);

// Errors inform users that something went wrong.
decl_error! {
	pub enum Error for Module<T: Trait> {
		NoneValue,
		StorageOverflow,
	}
}


impl<T: Trait> Printable for Error<T> {
	fn print(&self) {
		match self {
			Error::NoneValue => "Invalid Value".print(),
			Error::StorageOverflow => "Value Exceeded and Overflowed".print(),
			_ => "Invalid Error Case".print(),
		}
	}
}

// Dispatchable functions allows users to interact with the pallet and invoke state changes.
// These functions materialize as "extrinsics", which are often compared to transactions.
// Dispatchable functions must be annotated with a weight and must return a DispatchResult.
decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		type Error = Error<T>;

		fn deposit_event() = default;

		#[weight = 0]
		pub fn do_something(origin, something: u32) -> dispatch::DispatchResult {

			let who = ensure_signed(origin)?;

			// Writing from Native
			// Debug
 			frame_support::debug::native::debug!("called by {:?}", who);
 			// Info
			debug::info!("Request sent by: {:?}", who);
			debug::info!("The Value: {:?} will be updated on the storate", something);

			// Doing from WASM (paying Performance Price)
			frame_support::debug::RuntimeLogger::init();
    		frame_support::debug::debug!("called by {:?}", who);

			// Update storage.
			Something::put(something);

			// Emit an event.
			Self::deposit_event(RawEvent::SomethingStored(something, who));
			// Return a successful DispatchResult
			Ok(())
		}


		#[weight = 0]
		pub fn cause_error(origin) -> dispatch::DispatchResult {

			let _who = ensure_signed(origin)?;

			print("My Test Message");

			match Something::get() {
				None => {
					print(Error::<T>::NoneValue);
					Err(Error::<T>::NoneValue)?
				}
				Some(old) => {
					let new = old.checked_add(1).ok_or(
						{
							print(Error::<T>::StorageOverflow);
							Error::<T>::StorageOverflow
						})?;
					Something::put(new);
					Ok(())
				},
			}
		}

		/*
		#[weight = 0]
		pub fn cause_error(origin) -> dispatch::DispatchResult {
			let _who = ensure_signed(origin)?;

			// Read a value from storage.
			match Something::get() {
				// Return an error if the value has not been set.
				None => Err(Error::<T>::NoneValue)?,
				Some(old) => {
					// Increment the value read from storage; will error in the event of overflow.
					let new = old.checked_add(1).ok_or(Error::<T>::StorageOverflow)?;
					// Update the value in storage with the incremented result.
					Something::put(new);
					Ok(())
				},
			}
		}*/
	}
}
