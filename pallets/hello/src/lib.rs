#![cfg_attr(not(feature = "std"), no_std)]

use frame_support::{debug, decl_module, dispatch::DispatchResult};
use frame_system::{self as system, ensure_signed};
use sp_runtime::print;

#[cfg(test)]
mod tests;

pub trait Trait: system::Trait {}

decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {

		/// A function that says hello to the user by printing messages to the node log
		#[weight = 0]
		pub fn greeting(origin) -> DispatchResult {
			// Ensure that the caller is a regular keypair account
			let caller = ensure_signed(origin)?;

			// Print a message
			/*

    			Print function note: To actually see the printed messages, we need to use the
    			    flag -lruntime=debug when running the running node.

			 */

			print("Hello World - make sure your are using -lruntime=debug");
			// Inspecting a variable as well
			debug::info!("Request sent by: {:?}", caller);

			// Indicate that this call succeeded
			Ok(())
		}
	}
}
